[{*
/*    Please retain this copyright header in all versions of the software
 *
 *    Copyright (C) Josef A. Puckl | eComStyle.de
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see {http://www.gnu.org/licenses/}.
 */
*}]

[{capture name=billzip}]
    [{assign var="searchthezip" value="http://www.plz-postleitzahl.de/site.plz/search.html?c=plz&q=" }]
    [{assign var="billzip"      value=$edit->oxorder__oxbillzip->value|strip_tags|replace:" ":"" }]
    [{assign var="billcity"     value=$edit->oxorder__oxbillcity->value|strip_tags|strip|html_entity_decode}]
    [{assign var="billzipchk"   value="PLZ`$billzip`"}]
    [{assign var="billzipcity"  value=$billzipchk|oxmultilangassign|html_entity_decode}]

    <a href="[{$searchthezip}][{$billzip}]" target="_blank" onclick="window.open('[{$searchthezip}][{$billzip}]', 'ZipChecker', 'resizable=yes,status=no,scrollbars=yes,menubar=no,width=650,height=600');return false;">
        [{if $billzipcity|strpos:"Translation" or $billzipcity eq $billzipchk}]
            <span style="color: #8F8F8F;border: 1px solid #8F8F8F;padding: 0 1px;">[{oxmultilang ident="GENERAL_NODATA"}]</span>
        [{elseif $billzipcity|lower|replace:" ":"" eq $billcity|lower|replace:" ":""}]
            <span style="font-weight: bold; color: #16AF08;border: 1px solid #16AF08;padding: 0 1px;">OK[{*$billzipcity*}]</span>
        [{else}]
            <span style="font-weight: bold; color: #CE272F;border: 1px solid #CE272F;padding: 0 1px;">->&nbsp;[{$billzipcity}]</span>
        [{/if}]
    </a>
[{/capture}]
<span>[{ oxmultilang ident="USER_LIST_ZIP" }]&nbsp;[{$billzip}]:</span>&nbsp;[{$smarty.capture.billzip}]<hr>

[{$smarty.block.parent}]