[{*
/*    Please retain this copyright header in all versions of the software
 *
 *    Copyright (C) Josef A. Puckl | eComStyle.de
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see {http://www.gnu.org/licenses/}.
 */
*}]

[{capture name=delzip}]
    [{assign var="searchthezip" value="http://www.plz-postleitzahl.de/site.plz/search.html?c=plz&q=" }]
    [{assign var="delzip"       value=$edit->oxorder__oxdelzip->value|strip_tags|replace:" ":"" }]
    [{assign var="delcity"      value=$edit->oxorder__oxdelcity->value|strip_tags|strip|html_entity_decode}]
    [{assign var="delzipchk"    value="PLZ`$delzip`"}]
    [{assign var="delzipcity"   value=$delzipchk|oxmultilangassign|html_entity_decode}]

    <a href="[{$searchthezip}][{$delzip}]" target="_blank" onclick="window.open('[{$searchthezip}][{$delzip}]', 'www.plz-postleitzahl.de', 'resizable=yes,status=no,scrollbars=yes,menubar=no,width=650,height=600');return false;">
        [{if $delzipcity|strpos:"Translation" or $delzipcity eq $delzipchk}]
            <span style="color: #8F8F8F;border: 1px solid #8F8F8F;padding: 0 1px;">[{oxmultilang ident="GENERAL_NODATA"}]</span>
        [{elseif $delzipcity|lower|replace:" ":"" eq $delcity|lower|replace:" ":""}]
            <span style="font-weight: bold; color: #16AF08;border: 1px solid #16AF08;padding: 0 1px;">OK[{*$delzipcity*}]</span>
        [{else}]
            <span style="font-weight: bold; color: #CE272F;border: 1px solid #CE272F;padding: 0 1px;">->&nbsp;[{$delzipcity}]</span>
        [{/if}]
    </a>
[{/capture}]
<span>[{ oxmultilang ident="USER_LIST_ZIP" }]&nbsp;[{$delzip}]:</span>&nbsp;[{$smarty.capture.delzip}]<hr>

[{$smarty.block.parent}]