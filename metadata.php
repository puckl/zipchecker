<?php
/*    Please retain this copyright header in all versions of the software
 *
 *    Copyright (C) 2016  Josef A. Puckl | eComStyle.de
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see {http://www.gnu.org/licenses/}.
 */

$sMetadataVersion   = '2.0';
$aModule            = [
    'id'            => 'ecs_zipchecker',
    'title'         => '<strong style="color:#04B431;">e</strong><strong>ComStyle.de</strong>:  <i>ZipChecker</i>',
    'description'   => 'Stellt einen schnellen PLZ-Vergleich bei der Bestellung unter &Uuml;bersicht zur Verf&uuml;gung.',
    'version'       => '1.2.0',
    'thumbnail'     => 'ecs.png',
    'author'        => '<strong style="font-size: 17px;color:#04B431;">e</strong><strong style="font-size: 16px;">ComStyle.de</strong>',
    'email'         => 'info@ecomstyle.de',
    'url'           => 'http://ecomstyle.de',
    'extend' => [
    ],
    'blocks' => [
        ['template' => 'order_overview.tpl', 'block' => 'admin_order_overview_billingaddress',  'file' => 'views/admin/blocks/billzipcheck_order_overview.tpl'],
        ['template' => 'order_overview.tpl', 'block' => 'admin_order_overview_deliveryaddress', 'file' => 'views/admin/blocks/delzipcheck_order_overview.tpl'],
    ]
];
